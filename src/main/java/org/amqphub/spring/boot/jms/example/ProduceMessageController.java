package org.amqphub.spring.boot.jms.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class ProduceMessageController {

    @Autowired
    Sender jmsProducer;

    @GetMapping(value="/broker/send-msq/{str}")
    public ResponseEntity<?> sendMessage(@PathVariable("str") String str){
        log.info("Attempting Send message to Topic: "+ str);
        jmsProducer.send(str);
        return new ResponseEntity<String>(String.format("Mensagem enviado com sucesso :: %s", str), HttpStatus.OK);
    }
}
